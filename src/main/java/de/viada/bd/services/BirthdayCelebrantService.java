package de.viada.bd.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import de.viada.bd.model.User;

@Stateless
public class BirthdayCelebrantService {

	@Inject
	private UserService userService;
	
	public List<User> getUsers(){
		return userService.getBirthdayCelebrants();
	}
	
}
