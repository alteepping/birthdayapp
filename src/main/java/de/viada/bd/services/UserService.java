package de.viada.bd.services;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import de.viada.bd.model.User;

public class UserService {

	public List<User> getBirthdayCelebrants(){
		List<User> users = new ArrayList<>();
		users.add(new User(1, "Hans", "Hanson", LocalDate.now().minusYears(42)));
		users.add(new User(1, "Klara", "Klarson", LocalDate.now().minusYears(31)));
		users.add(new User(1, "Peter", "Peterson", LocalDate.now().minusYears(17)));
		users.add(new User(1, "Hermine", "Herminson", LocalDate.now().minusYears(3)));
		return users;
	}
}
