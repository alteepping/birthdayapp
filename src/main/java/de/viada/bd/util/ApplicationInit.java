package de.viada.bd.util;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("api/v1")
public class ApplicationInit extends Application{

}
