package de.viada.bd.model;

import java.time.LocalDate;

public class User {

	private long id;
	private String firstName;
	private String lastName;
	private LocalDate birthday;
	
	public User(){
	}

	public User(long id, String firstName, String lastname, LocalDate birthday){
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastname;
		this.birthday = birthday;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getfirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public LocalDate getBirthday() {
		return birthday;
	}

	public void setBirthday(LocalDate birthday) {
		this.birthday = birthday;
	}
	
	
}
