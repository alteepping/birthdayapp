package de.viada.bd.webservices;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import de.viada.bd.model.User;
import de.viada.bd.services.BirthdayCelebrantService;


@Path("birthdaycelebrants")
public class BirthdayCelebrants {

	@Inject
	private BirthdayCelebrantService birthdayCelebrantService ;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<User> getCelebrants(){
		return birthdayCelebrantService.getUsers();
	}
	
}
